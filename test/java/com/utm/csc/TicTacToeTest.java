package testing;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import game.TicTacToe;

public class TicTacToeTest {
	private TicTacToe board;

	@Before
	public void setUp() throws Exception {
		board = new TicTacToe();
		board.initBoard();
	}

	@Test
	public void getGameBoardTest() {
		char[][] gameBoard = board.getGameBoard();
		char[][] testBoard = new char[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				testBoard[i][j] = '-';
			}
		}
		assertArrayEquals(gameBoard, testBoard);
	}

	@Test
	public void checkBoundsTest() {
		boolean test = board.checkBounds(5, 1);
		boolean expected = true;
		assertEquals(test, expected);
	}


	@Test
	public void setPositionTest() {
		board.setPosition('X', 1, 1);
		char[][] test = new char[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				test[i][j] = '-';
			}
		}
		test[1][1] = 'X';
		assertArrayEquals(test,board.getGameBoard());
	}

	@Test
	public void checkOccupiedTest() {
		board.setPosition('X', 1, 1);
		boolean isNotOccupied = false;
		boolean isItActually = board.checkOccupied(1, 1);
		assertEquals(isNotOccupied, isItActually);
	}

	@Test
	public void hasWonTest() {
		board.setPosition('X', 0, 0);
		board.setPosition('X', 1, 1);
		board.setPosition('X', 2, 2);
		boolean haveIWon = board.hasWon();
		boolean status = true;
		
		assertEquals(haveIWon, status);
	}

	@Test
	public void checkRowsForWinTest() {
		board.setPosition('X', 0, 0);
		board.setPosition('X', 0, 1);
		board.setPosition('X', 0, 2);
		boolean haveIWon = board.checkRowsForWin();
		boolean status = true;
		assertEquals(haveIWon, status);
	}
	
	@Test
	public void checkColumnsForWinTest(){
		board.setPosition('X', 0, 0);
		board.setPosition('X', 1, 0);
		board.setPosition('X', 2, 0);
		boolean haveIWon = board.checkColumnsForWin();
		boolean status = true;
		assertEquals(haveIWon, status);
	}
	
	@Test
	public void checkDiagonalsForWinTest(){
		board.setPosition('X', 0, 0);
		board.setPosition('X', 1, 1);
		board.setPosition('X', 2, 2);
		boolean haveIWon = board.checkDiagonalsForWin();
		boolean status = true;
		assertEquals(haveIWon, status);
	}

	@After
	public void tearDown() throws Exception {
		board = null;
	}

}
