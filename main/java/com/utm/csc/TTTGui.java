package game;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JLabel;

public class TTTGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	TicTacToe game;
	int play = 0;
	char player;
	boolean hasWon = false;
	JButton btnEndGame = new JButton("End Game");
	JLabel lblOutput = new JLabel("");
	JLabel lblStatus = new JLabel("");
	JButton btnPlayer = new JButton("Button 1");
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TTTGui frame = new TTTGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public char getPlayer(int num) {
		char player;
		if (num % 2 == 0) {
			player = 'X';
		} else {
			player = 'Y';
		}
		return player;
	}
	public String nextPlayer(String currentPlayer){
		String nextPlayer = null;
		if (currentPlayer.equals("X")){
			nextPlayer = "Y";
		} else {
			nextPlayer = "X";
		}
		return nextPlayer;
	}

	public void doStuff(JButton button, char plyer, GridBagConstraints gbc) {
		if (play < 9) {
			String currentPlayer = Character.toString(plyer);
			game.setPosition(plyer, gbc.gridx, gbc.gridy);
			button.setText(currentPlayer);
			lblOutput.setText("It is now: " + nextPlayer(currentPlayer) + " turn to play.");
			hasWon = game.hasWon();
			btnPlayer.setText(nextPlayer(currentPlayer) + " turn");
			if (hasWon) {
				lblStatus.setText(currentPlayer + " has won.");
				
				lblOutput.setText("Game over.");
				play = 10;
			} 
			
			play++;
			button.setEnabled(false);
		}
		if (play == 9){
			lblStatus.setText("Game has tied.");
			lblOutput.setText("");
			
		}
	}

	public TTTGui() {
		setTitle("Tic Tac Toe");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 406, 394);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel panel = new JPanel();
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 73, 73, 73, 0 };
		gbl_panel.rowHeights = new int[] { 23, 23, 23, 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		JButton btnOne = new JButton(" ");
		btnOne.setFont(new Font("Tahoma", Font.PLAIN, 18));

		GridBagConstraints gbc_btnOne = new GridBagConstraints();
		gbc_btnOne.insets = new Insets(0, 0, 5, 5);
		gbc_btnOne.gridx = 0;
		gbc_btnOne.ipady = 50;
		gbc_btnOne.ipadx = 50;
		gbc_btnOne.gridy = 0;
		btnOne.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				doStuff(btnOne, getPlayer(play), gbc_btnOne);
			}
		});
		panel.add(btnOne, gbc_btnOne);

		JButton btnTwo = new JButton(" ");

		btnTwo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnTwo = new GridBagConstraints();
		gbc_btnTwo.anchor = GridBagConstraints.BELOW_BASELINE;
		gbc_btnTwo.insets = new Insets(0, 0, 5, 5);
		gbc_btnTwo.gridx = 1;
		gbc_btnTwo.gridy = 0;
		gbc_btnTwo.ipady = 50;
		gbc_btnTwo.ipadx = 50;
		btnTwo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnTwo, getPlayer(play), gbc_btnTwo);
			}
		});
		panel.add(btnTwo, gbc_btnTwo);

		JButton btnThree = new JButton(" ");

		btnThree.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnThree = new GridBagConstraints();
		gbc_btnThree.insets = new Insets(0, 0, 5, 0);
		gbc_btnThree.gridx = 2;
		gbc_btnThree.gridy = 0;
		gbc_btnThree.ipady = 50;
		gbc_btnThree.ipadx = 50;
		btnThree.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnThree, getPlayer(play), gbc_btnThree);
			}
		});
		panel.add(btnThree, gbc_btnThree);

		JButton btnFour = new JButton(" ");

		btnFour.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnFour = new GridBagConstraints();
		gbc_btnFour.insets = new Insets(0, 0, 5, 5);
		gbc_btnFour.gridx = 0;
		gbc_btnFour.gridy = 1;
		gbc_btnFour.ipady = 50;
		gbc_btnFour.ipadx = 50;
		btnFour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnFour, getPlayer(play), gbc_btnFour);
			}
		});
		panel.add(btnFour, gbc_btnFour);

		JButton btnFive = new JButton(" ");

		btnFive.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnFive = new GridBagConstraints();
		gbc_btnFive.insets = new Insets(0, 0, 5, 5);
		gbc_btnFive.gridx = 1;
		gbc_btnFive.gridy = 1;
		gbc_btnFive.ipady = 50;
		gbc_btnFive.ipadx = 50;
		btnFive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnFive, getPlayer(play), gbc_btnFive);
			}
		});
		panel.add(btnFive, gbc_btnFive);

		JButton btnSix = new JButton(" ");

		btnSix.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnSix = new GridBagConstraints();
		gbc_btnSix.insets = new Insets(0, 0, 5, 0);
		gbc_btnSix.gridx = 2;
		gbc_btnSix.gridy = 1;
		gbc_btnSix.ipady = 50;
		gbc_btnSix.ipadx = 50;
		btnSix.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnSix, getPlayer(play), gbc_btnSix);
			}
		});
		panel.add(btnSix, gbc_btnSix);

		JButton btnSeven = new JButton(" ");

		btnSeven.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnSeven = new GridBagConstraints();
		gbc_btnSeven.anchor = GridBagConstraints.SOUTH;
		gbc_btnSeven.insets = new Insets(0, 0, 5, 5);
		gbc_btnSeven.gridx = 0;
		gbc_btnSeven.gridy = 2;
		gbc_btnSeven.ipady = 50;
		gbc_btnSeven.ipadx = 50;
		btnSeven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnSeven, getPlayer(play), gbc_btnSeven);
			}
		});
		panel.add(btnSeven, gbc_btnSeven);

		JButton btnEight = new JButton(" ");

		btnEight.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnEight = new GridBagConstraints();
		gbc_btnEight.insets = new Insets(0, 0, 5, 5);
		gbc_btnEight.gridx = 1;
		gbc_btnEight.gridy = 2;
		gbc_btnEight.ipady = 50;
		gbc_btnEight.ipadx = 50;
		btnEight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnEight, getPlayer(play), gbc_btnEight);
			}
		});
		panel.add(btnEight, gbc_btnEight);

		JButton btnNine = new JButton(" ");

		btnNine.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GridBagConstraints gbc_btnNine = new GridBagConstraints();
		gbc_btnNine.insets = new Insets(0, 0, 5, 0);
		gbc_btnNine.gridx = 2;
		gbc_btnNine.gridy = 2;
		gbc_btnNine.ipady = 50;
		gbc_btnNine.ipadx = 50;
		btnNine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doStuff(btnNine, getPlayer(play), gbc_btnNine);
			}
		});
		panel.add(btnNine, gbc_btnNine);

		btnOne.setEnabled(false);
		btnTwo.setEnabled(false);
		btnThree.setEnabled(false);
		btnFour.setEnabled(false);
		btnFive.setEnabled(false);
		btnSix.setEnabled(false);
		btnSeven.setEnabled(false);
		btnEight.setEnabled(false);
		btnNine.setEnabled(false);

		btnEndGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOne.setEnabled(false);
				btnTwo.setEnabled(false);
				btnThree.setEnabled(false);
				btnFour.setEnabled(false);
				btnFive.setEnabled(false);
				btnSix.setEnabled(false);
				btnSeven.setEnabled(false);
				btnEight.setEnabled(false);
				btnNine.setEnabled(false);

			}
		});
		GridBagConstraints gbc_btnEndGame = new GridBagConstraints();
		gbc_btnEndGame.anchor = GridBagConstraints.NORTH;
		gbc_btnEndGame.insets = new Insets(0, 0, 5, 5);
		gbc_btnEndGame.gridx = 0;
		gbc_btnEndGame.gridy = 3;
		panel.add(btnEndGame, gbc_btnEndGame);

		btnPlayer.setEnabled(false);
		GridBagConstraints gbc_btnPlayer = new GridBagConstraints();
		gbc_btnPlayer.insets = new Insets(0, 0, 5, 5);
		gbc_btnPlayer.gridx = 1;
		gbc_btnPlayer.gridy = 3;
		panel.add(btnPlayer, gbc_btnPlayer);

		JButton btnNewGame = new JButton("New Game");
		btnNewGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnOne.setEnabled(true);
				btnOne.setText(" ");
				btnTwo.setEnabled(true);
				btnTwo.setText(" ");
				btnThree.setEnabled(true);
				btnThree.setText(" ");
				btnFour.setEnabled(true);
				btnFour.setText(" ");
				btnFive.setEnabled(true);
				btnFive.setText(" ");
				btnSix.setEnabled(true);
				btnSix.setText(" ");
				btnSeven.setEnabled(true);
				btnSeven.setText(" ");
				btnEight.setEnabled(true);
				btnEight.setText(" ");
				btnNine.setEnabled(true);
				btnNine.setText(" ");
				lblOutput.setText(" ");
				lblStatus.setText(" ");
				game = new TicTacToe();
				play = 0;

			}
		});
		GridBagConstraints gbc_btnNewGame = new GridBagConstraints();
		gbc_btnNewGame.insets = new Insets(0, 0, 5, 0);
		gbc_btnNewGame.gridx = 2;
		gbc_btnNewGame.gridy = 3;
		panel.add(btnNewGame, gbc_btnNewGame);

		GridBagConstraints gbc_lblOutput = new GridBagConstraints();
		gbc_lblOutput.insets = new Insets(0, 0, 5, 0);
		gbc_lblOutput.gridwidth = 3;
		gbc_lblOutput.gridx = 0;
		gbc_lblOutput.gridy = 4;
		panel.add(lblOutput, gbc_lblOutput);

		GridBagConstraints gbc_lblStatus = new GridBagConstraints();
		gbc_lblStatus.gridwidth = 3;
		gbc_lblStatus.insets = new Insets(0, 0, 0, 5);
		gbc_lblStatus.gridx = 0;
		gbc_lblStatus.gridy = 5;
		panel.add(lblStatus, gbc_lblStatus);
	}

}
